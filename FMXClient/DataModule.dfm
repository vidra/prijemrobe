object DMGlavni: TDMGlavni
  Height = 299
  Width = 570
  PixelsPerInch = 96
  object RESTClientPods: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'utf-8, *;q=0.8'
    BaseURL = 'http://31.45.234.138:61116/datasnap/rest/tsm/getpods'
    Params = <>
    Left = 16
    Top = 16
  end
  object RESTRequestPods: TRESTRequest
    Client = RESTClientPods
    Params = <>
    Response = RESTResponsePods
    SynchronizedEvents = False
    Left = 48
    Top = 16
  end
  object RESTResponsePods: TRESTResponse
    RootElement = 'result'
    Left = 80
    Top = 16
  end
  object RESTResponsePodsDataSetAdapter: TRESTResponseDataSetAdapter
    Dataset = FDMemTablePods
    FieldDefs = <>
    ResponseJSON = RESTResponsePods
    Left = 112
    Top = 16
  end
  object FDMemTablePods: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvUpdateChngFields, uvUpdateMode, uvLockMode, uvLockPoint, uvLockWait, uvRefreshMode, uvFetchGeneratorsPoint, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable, uvAutoCommitUpdates]
    UpdateOptions.LockWait = True
    UpdateOptions.FetchGeneratorsPoint = gpNone
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 144
    Top = 16
    object FDMemTablePodspod: TIntegerField
      FieldName = 'pod'
    end
    object FDMemTablePodsnaz: TStringField
      FieldName = 'naz'
      Size = 200
    end
  end
  object fdConsDiventa: TFDConnection
    Params.Strings = (
      'Database=C:\Databases\SQLite\PrijemRobe\PrijemRobe.db'
      'DriverID=SQLite')
    LoginPrompt = False
    AfterConnect = fdConsDiventaAfterConnect
    BeforeConnect = fdConsDiventaBeforeConnect
    Left = 32
    Top = 152
  end
  object fdqPodMaxId: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'SELECT NVL(MAX(Id), 0) AS MAXID FROM Pod')
    Left = 104
    Top = 152
  end
  object fdqPodUpdate: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'UPDATE Pod SET'
      'Pod = :Pod,'
      'Naziv = :Naziv'
      'WHERE Id = :Id')
    Left = 104
    Top = 200
    ParamData = <
      item
        Name = 'POD'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'NAZIV'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdqPodInsert: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'INSERT INTO Pod (Id, Pod, Naziv)'
      'VALUES (:ID, :Pod, :Naziv)')
    Left = 144
    Top = 152
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'POD'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'NAZIV'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdqPodDelete: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'DELETE FROM Pod WHERE Id = :Id')
    Left = 144
    Top = 200
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdqPodSelect: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'SELECT Pod, Naziv'
      'FROM Pod'
      'WHERE Id = :Id')
    Left = 184
    Top = 152
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdqPodSelectAll: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'SELECT * FROM POD'
      'ORDER BY POD')
    Left = 184
    Top = 200
  end
  object RESTClientDoks: TRESTClient
    BaseURL = 'http://31.45.234.138:61116/datasnap/rest/tsm/getdoks/2021/1'
    Params = <>
    Left = 16
    Top = 72
  end
  object RESTRequestDoks: TRESTRequest
    Client = RESTClientDoks
    Params = <>
    Response = RESTResponseDoks
    SynchronizedEvents = False
    Left = 48
    Top = 72
  end
  object RESTResponseDoks: TRESTResponse
    Left = 80
    Top = 72
  end
  object RESTResponseDoksDataSetAdapter: TRESTResponseDataSetAdapter
    Dataset = FDMemTableDoks
    FieldDefs = <>
    Response = RESTResponseDoks
    Left = 112
    Top = 72
  end
  object FDMemTableDoks: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvUpdateChngFields, uvUpdateMode, uvLockMode, uvLockPoint, uvLockWait, uvRefreshMode, uvFetchGeneratorsPoint, uvCheckRequired, uvCheckReadOnly, uvCheckUpdatable]
    UpdateOptions.LockWait = True
    UpdateOptions.FetchGeneratorsPoint = gpNone
    UpdateOptions.CheckRequired = False
    Left = 144
    Top = 72
  end
  object fdqDokMaxId: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'SELECT NVL(MAX(Id), 0) AS MAXID FROM Dok')
    Left = 256
    Top = 152
  end
  object fdqDokInsert: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'INSERT INTO Dok (Id, God, Pod, Org, Dok, Br, '
      'Par, Datk, Status, Oper, Vrijeme)'
      'VALUES (:Id, :God, :Pod, :Org, :Dok, :Br, '
      ':Par, :Datk, :Status, :Oper, :Vrijeme)')
    Left = 296
    Top = 152
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'GOD'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'POD'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'ORG'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DOK'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'BR'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'PAR'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DATK'
        DataType = ftDate
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'STATUS'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'OPER'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'VRIJEME'
        DataType = ftDateTime
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdqDokSelect: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'SELECT God, Pod, Org, Dok, Br, '
      'Par, Datk, Status, Oper, Vrijeme'
      'FROM Dok'
      'WHERE Id = :Id')
    Left = 336
    Top = 152
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdqDokUpdate: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'UPDATE Dok SET'
      'God = :God, '
      'Pod = :Pod, '
      'Org = :Org, '
      'Dok = :Dok, '
      'Br = :Br, '
      'Par = :Par, '
      'Datk = :Datk, '
      'Status = :Status, '
      'Oper = :Oper, '
      'Vrijeme = :Vrijeme'
      'WHERE Id = :Id')
    Left = 256
    Top = 200
    ParamData = <
      item
        Name = 'GOD'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'POD'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'ORG'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DOK'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'BR'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'PAR'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DATK'
        DataType = ftDate
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'STATUS'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'OPER'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'VRIJEME'
        DataType = ftDateTime
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdqDokDelete: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'DELETE FROM Dok WHERE Id = :Id')
    Left = 296
    Top = 200
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdqDokSelectAll: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'SELECT * FROM DOK'
      'ORDER BY DATK')
    Left = 336
    Top = 200
  end
  object fdqStavkeMaxId: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'SELECT NVL(MAX(Id), 0) AS MAXID FROM Stavke')
    Left = 256
    Top = 48
  end
  object fdqStavkeInsert: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'INSERT INTO Stavke(Id, DokId, Pod, St, Ul, Nabcij, '
      'Vrnab, MUl, MNabcij, '
      'MVrnab, Mat, Vmj, Naz, Napomena, BarCode,'
      ' Oper, Vrijeme)'
      'VALUES (:Id, :DokId, :Pod, :St, :Ul, :Nabcij, '
      ':Vrnab, :MUl, :MNabcij, '
      ':MVrnab, :Mat, :Vmj, :Naz, :Napomena, :BarCode, '
      ':Oper, :Vrijeme)')
    Left = 296
    Top = 48
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'DOKID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'POD'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'ST'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'UL'
        DataType = ftCurrency
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'NABCIJ'
        DataType = ftCurrency
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'VRNAB'
        DataType = ftCurrency
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'MUL'
        DataType = ftCurrency
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'MNABCIJ'
        DataType = ftCurrency
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'MVRNAB'
        DataType = ftCurrency
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'MAT'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'VMJ'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'NAZ'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'NAPOMENA'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'BARCODE'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'OPER'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'VRIJEME'
        DataType = ftDateTime
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdqStavkeSelect: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'SELECT Id, DokId, Pod, St, Ul, Nabcij, '
      'Vrnab, MUl, MNabcij, '
      'MVrnab, Mat, Vmj, Naz, Napomena, BarCode, '
      'Oper, Vrijeme'
      'FROM Stavke'
      'WHERE Id = :Id')
    Left = 336
    Top = 48
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdqStavkeUpdate: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'UPDATE Stavke SET'
      'DokId = :DokId, '
      'Pod = :Pod,'
      'St = :St, '
      'Ul = :Ul, '
      'MNabcij = :MNabcij, '
      'MVrnab = :MVrnab, '
      'MUl = :MUl, '
      'Nabcij = :Nabcij, '
      'Mat = :Mat, '
      'Vmj = :Vmj, '
      'Naz = :Naz,'
      'Napomena = :Napomena, '
      'BarCode = :BarCode,'
      'Oper = :Oper, '
      'Vrijeme = :Vrijeme'
      'WHERE Id = :Id')
    Left = 256
    Top = 96
    ParamData = <
      item
        Name = 'DOKID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'POD'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'ST'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'UL'
        DataType = ftCurrency
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'MNABCIJ'
        ParamType = ptInput
      end
      item
        Name = 'MVRNAB'
        ParamType = ptInput
      end
      item
        Name = 'MUL'
        ParamType = ptInput
      end
      item
        Name = 'NABCIJ'
        DataType = ftCurrency
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'MAT'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'VMJ'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'NAZ'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'NAPOMENA'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'BARCODE'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'OPER'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'VRIJEME'
        DataType = ftDateTime
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdqStavkeDelete: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'DELETE FROM Stavke WHERE Id = :Id')
    Left = 296
    Top = 96
    ParamData = <
      item
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object fdqStavkeSelectAll: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'SELECT * FROM STAVKE'
      'WHERE DOKID = :DOKID'
      'ORDER BY ST')
    Left = 336
    Top = 96
    ParamData = <
      item
        Name = 'DOKID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
  end
  object RESTClientStavke: TRESTClient
    BaseURL = 
      'http://31.45.234.138:61116/datasnap/rest/tsm/getdokstavke/2021/1' +
      '/2/351/1'
    Params = <>
    Left = 256
    Top = 16
  end
  object RESTRequestStavke: TRESTRequest
    Client = RESTClientStavke
    Params = <>
    Response = RESTResponseStavke
    SynchronizedEvents = False
    Left = 296
    Top = 16
  end
  object RESTResponseStavke: TRESTResponse
    Left = 336
    Top = 16
  end
  object RESTClientSetDokStavke: TRESTClient
    BaseURL = 
      'http://31.45.234.138:61116/datasnap/rest/tsm/setdokstavke/2021/1' +
      '/2/351/1/tekst'
    Params = <>
    Left = 384
    Top = 16
  end
  object RESTRequestSetDokStavke: TRESTRequest
    Client = RESTClientSetDokStavke
    Method = rmPOST
    Params = <>
    Response = RESTResponseSetDokStavke
    SynchronizedEvents = False
    Left = 424
    Top = 16
  end
  object RESTResponseSetDokStavke: TRESTResponse
    Left = 464
    Top = 16
  end
  object FDListViewPod: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'SELECT * FROM POD'
      'ORDER BY POD')
    Left = 74
    Top = 249
    object FDListViewPodId: TIntegerField
      FieldName = 'Id'
      Origin = 'Id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDListViewPodPod: TIntegerField
      FieldName = 'Pod'
      Origin = 'Pod'
    end
    object FDListViewPodNaziv: TWideMemoField
      FieldName = 'Naziv'
      Origin = 'Naziv'
      BlobType = ftWideMemo
    end
  end
  object dsListViewPod: TDataSource
    DataSet = FDListViewPod
    Left = 103
    Top = 248
  end
  object FDListViewDok: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'SELECT Id,God,Pod,ORG,Dok,Br,Par,Datk,'
      'Status,Oper, Vrijeme, '
      
        'God || '#39'\'#39' || Pod || '#39'\'#39' || ORG || '#39'\'#39' || Dok || '#39'\'#39' || Br Dokum' +
        'ent'
      'FROM DOK'
      'WHERE POD = :POD'
      'ORDER BY DATK')
    Left = 256
    Top = 248
    ParamData = <
      item
        Name = 'POD'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDListViewDokId: TIntegerField
      FieldName = 'Id'
      Origin = 'Id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDListViewDokGod: TIntegerField
      FieldName = 'God'
      Origin = 'God'
    end
    object FDListViewDokPod: TIntegerField
      FieldName = 'Pod'
      Origin = 'Pod'
    end
    object FDListViewDokORG: TIntegerField
      FieldName = 'ORG'
      Origin = 'ORG'
    end
    object FDListViewDokDok: TIntegerField
      FieldName = 'Dok'
      Origin = 'Dok'
    end
    object FDListViewDokBr: TIntegerField
      FieldName = 'Br'
      Origin = 'Br'
    end
    object FDListViewDokPar: TIntegerField
      FieldName = 'Par'
      Origin = 'Par'
    end
    object FDListViewDokDatk: TDateField
      FieldName = 'Datk'
      Origin = 'Datk'
    end
    object FDListViewDokStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'Status'
    end
    object FDListViewDokOper: TIntegerField
      FieldName = 'Oper'
      Origin = 'Oper'
    end
    object FDListViewDokVrijeme: TDateTimeField
      FieldName = 'Vrijeme'
      Origin = 'Vrijeme'
    end
    object FDListViewDokDokument: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'Dokument'
      Origin = 'Dokument'
      ProviderFlags = []
      ReadOnly = True
      Size = 32767
    end
  end
  object FDListViewStavka: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'SELECT * FROM STAVKE'
      'WHERE DOKID = :DOKID'
      'ORDER BY ST')
    Left = 288
    Top = 248
    ParamData = <
      item
        Name = 'DOKID'
        DataType = ftInteger
        ParamType = ptInput
        Value = Null
      end>
    object FDListViewStavkaId: TIntegerField
      FieldName = 'Id'
      Origin = 'Id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDListViewStavkaDokId: TIntegerField
      FieldName = 'DokId'
      Origin = 'DokId'
    end
    object FDListViewStavkaPod: TIntegerField
      FieldName = 'Pod'
      Origin = 'Pod'
    end
    object FDListViewStavkaSt: TIntegerField
      FieldName = 'St'
      Origin = 'St'
    end
    object FDListViewStavkaUl: TFloatField
      FieldName = 'Ul'
      Origin = 'Ul'
    end
    object FDListViewStavkaNabcij: TFloatField
      FieldName = 'Nabcij'
      Origin = 'Nabcij'
    end
    object FDListViewStavkaVrnab: TFloatField
      FieldName = 'Vrnab'
      Origin = 'Vrnab'
    end
    object FDListViewStavkaMat: TIntegerField
      FieldName = 'Mat'
      Origin = 'Mat'
    end
    object FDListViewStavkaVmj: TWideMemoField
      FieldName = 'Vmj'
      Origin = 'Vmj'
      BlobType = ftWideMemo
    end
    object FDListViewStavkaNaz: TWideMemoField
      FieldName = 'Naz'
      Origin = 'Naz'
      BlobType = ftWideMemo
    end
    object FDListViewStavkaNapomena: TWideMemoField
      FieldName = 'Napomena'
      Origin = 'Napomena'
      BlobType = ftWideMemo
    end
    object FDListViewStavkaBarCode: TWideMemoField
      FieldName = 'BarCode'
      Origin = 'BarCode'
      BlobType = ftWideMemo
    end
    object FDListViewStavkaStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'Status'
    end
    object FDListViewStavkaOper: TIntegerField
      FieldName = 'Oper'
      Origin = 'Oper'
    end
    object FDListViewStavkaVrijeme: TDateTimeField
      FieldName = 'Vrijeme'
      Origin = 'Vrijeme'
    end
  end
  object fdqVerzija: TFDQuery
    Connection = fdConsDiventa
    SQL.Strings = (
      'select *'
      'from verzija where id = (SELECT NVL(MAX(Id), 0) FROM Verzija)')
    Left = 432
    Top = 152
  end
end
