unit untObjekti;

interface

uses System.Generics.Collections;

  const
    DebugOn = False;

  type TPoduzece = class
    private
      FId: Integer;
      FPod: Integer;
      FNaz: String;
    published
      property Id: Integer read FId write FId;
      property Pod: Integer read FPod write FPod;
      property Naziv: String read FNaz write FNaz;
  end;

  TListaPoduzeca = TList<TPoduzece>;

  IPoduzeceData = interface
    function PoduzeceCreate(aValue: TPoduzece): integer;
    function PoduzeceRead(id: integer; out aValue: TPoduzece): boolean;
    function PoduzeceUpdate(aValue: TPoduzece): boolean;
    function PoduzeceDelete(id: integer): boolean;
    procedure PoduzeceList(aList: TListaPoduzeca);
  end;

  type TDokument = class
    private
      FId: Integer;
      FGod: Integer;
      FPod: Integer;
      FOrg: Integer;
      FDok: Integer;
      FBr: Integer;
      FPar: Integer;
      FDatk: TDate;
      FStatus: Integer;
      FOper: Integer;
      FVrijeme: TDateTime;

    published
      property Id: Integer read FId write FId;
      property God: Integer read FGod write FGod;
      property Pod: Integer read FPod write FPod;
      property Org: Integer read FOrg write FOrg;
      property Dok: Integer read FDok write FDok;
      property Br: Integer read FBr write FBr;
      property Par: Integer read FPar write FPar;
      property Datk: TDate read FDatk write FDatk;
      property Status: Integer read FStatus write FStatus;
      property Oper: Integer read FOper write FOper;
      property Vrijeme: TDateTime read FVrijeme write FVrijeme;

  end;

  TListaDokumenata = TList<TDokument>;

  IDokumentData = interface
    function DokumentCreate(aValue: TDokument): integer;
    function DokumentRead(id: integer; out aValue: TDokument): boolean;
    function DokumentUpdate(aValue: TDokument): boolean;
    function DokumentDelete(id: integer): boolean;
    procedure DokumentList(aList: TListaDokumenata);
  end;

  type TStavke = class
  private
    FId: Integer;
    FDokId: Integer;
    FPod: Integer;
    FSt: Integer;
    FUl: Currency;
    FNabcij: Currency;
    FVrnab: Currency;
    FMUl: Currency;
    FMNabcij: Currency;
    FMVrnab: Currency;
    FMat: Integer;
    FVmj: String;
    FNaz: String;
    FNapomena: String;
    FBarCode: String;
    FStatus: Integer;
    FOper: Integer;
    FVrijeme: TDateTime;

    
  published
    property Id: Integer read FId write FId;
    property DokId: Integer read FDokId write FDokId;
    property Pod: Integer read FPod write FPod;
    property St: Integer read FSt write FSt;
    property Ul: Currency read FUl write FUl;
    property Nabcij: Currency read FNabcij write FNabcij;
    property Vrnab: Currency read FVrnab write FVrnab;
    property MUl: Currency read FMUl write FMUl;
    property MNabcij: Currency read FMNabcij write FMNabcij;
    property MVrnab: Currency read FMVrnab write FMVrnab;
    property Mat: Integer read FMat write FMat;
    property Vmj: String read FVmj write FVmj;
    property Naz: String read FNaz write FNaz;
    property Napomena: String read FNapomena write FNapomena;
    property BarCode: String read FBarCode write FBarCode;
    property Status: Integer read FStatus write FStatus;
    property Oper: Integer read FOper write FOper;
    property Vrijeme: TDateTime read FVrijeme write FVrijeme;

  end;

  TListaStavki = TList<TStavke>;

  IStavkeData = interface
    function StavkeCreate(aValue: TStavke): integer;
    function StavkeRead(id: integer; out aValue: TStavke): boolean;
    function StavkeUpdate(aValue: TStavke): boolean;
    function StavkeDelete(id: integer): boolean;
    procedure StavkeList(ADokId: Integer; aList: TListaStavki);
  end;

var
  dictDokumenti: TDictionary<String, Integer>;


implementation

{ Poduzece }



end.
