unit uTestGUI;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls, Data.Bind.Controls, FMX.Layouts,
  Fmx.Bind.Navigator,
  DataModule, Data.Bind.Components, Data.Bind.DBScope, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.EngExt,
  Fmx.Bind.DBEngExt, FMX.ListView;

type
  TFrmTestGUI = class(TForm)
    TextPod: TText;
    TextNaziv: TText;
    lblPoruka: TLabel;
    lblStatus: TLabel;
    btnRefresh: TButton;
    lblDataSourceStatus: TLabel;
    Button1: TButton;
    Button2: TButton;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    Button3: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnRefreshClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    procedure SetGUI;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmTestGUI: TFrmTestGUI;

implementation

{$R *.fmx}

uses
  System.JSON
  , untObjekti
  ;

procedure TFrmTestGUI.btnRefreshClick(Sender: TObject);
var
  LResult: TJSONArray;
  LJsonResponse: TJSONObject;
  jv: TJSONValue;
  Pod: TPoduzece;
  Pods: TListaPoduzeca;
  I: Integer;
begin
  Pods := TListaPoduzeca.Create;
  DMGlavni.PoduzeceList(Pods);
  DMGlavni.RESTRequestPods.Execute;
  // we got proper response from server
  if DMGlavni.RESTRequestPods.Response.StatusCode = 200 then
  begin
    for I := Pods.Count - 1 downto 0 do
    begin
      DMGlavni.PoduzeceDelete(Pods[I].Id);
      Pods[I].Free;
    end;
    Pods.Clear;
    Pods.Free;
    LJsonResponse := TJSONObject.ParseJSONValue(DMGlavni.RESTResponsePods.Content) as TJSONObject;
    LResult := LJsonResponse.GetValue('result') as TJSONArray;
    jv := LResult.Items[0];
    LResult := jv.GetValue<TJSONArray>('Poduzece');
    for jv in LResult do begin
      Pod := TPoduzece.Create;
      try
        Pod.Pod := jv.GetValue<Integer>('pod');
        Pod.Naziv := jv.GetValue<string>('naz');
        DMGlavni.PoduzeceCreate(Pod);
      except
        On E:Exception do
          ShowMessage('NapuniPoduzeca: ' + E.Message);
      end;
    end;
  end;
  DMGlavni.FDListViewPod.Refresh;
  SetGUI;
end;

procedure TFrmTestGUI.SetGUI;
begin
  lblStatus.Text := ('DMGlavni.FDListViewPod count: ' + DMGlavni.FDListViewPod.RecordCount.ToString);
  TextPod.Text := DMGlavni.FDListViewPod.FieldByName('pod').AsString;
  TextNaziv.Text := DMGlavni.FDListViewPod.FieldByName('Naziv').AsString;
  lblDataSourceStatus.Text := 'DMGlavni.FDListViewPod.Active = ' + BoolToStr(DMGlavni.FDListViewPod.Active, True);
end;

procedure TFrmTestGUI.Button1Click(Sender: TObject);
begin
  DMGlavni.FDListViewPod.Next;
  SetGUI;
end;

procedure TFrmTestGUI.Button2Click(Sender: TObject);
begin
  DMGlavni.FDListViewPod.Prior;
  SetGUI;
end;

procedure TFrmTestGUI.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DMGlavni.FDListViewPod.Close;
  DMGlavni.fdConsDiventa.Close;
end;

procedure TFrmTestGUI.FormShow(Sender: TObject);
begin
  DMGlavni.fdConsDiventa.Open;
  DMGlavni.FDListViewPod.Open;
  SetGUI;
end;

end.
