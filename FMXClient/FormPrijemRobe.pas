unit FormPrijemRobe;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Graphics, FMX.Forms, FMX.Dialogs, FMX.TabControl, System.Actions, FMX.ActnList,
  FMX.Objects, FMX.StdCtrls, FMX.Controls.Presentation, FMX.ListView.Types,
  FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListView,
  System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components, Data.Bind.DBScope
  , untObjekti
  , DataModule
  , FMX.Edit, FMX.Layouts, FMX.ListBox
  , uSettings
  ;

type
  TPrijemRobeForm = class(TForm)
    ActionList1: TActionList;
    PreviousTabAction1: TPreviousTabAction;
    TitleAction: TControlAction;
    NextTabAction1: TNextTabAction;
    TopToolBar: TToolBar;
    btnBack: TSpeedButton;
    ToolBarLabel: TLabel;
    TabControl1: TTabControl;
    tabPoduzece: TTabItem;
    BottomToolBar: TToolBar;
    sbAddPod: TSpeedButton;
    tabDokumenti: TTabItem;
    listViewDok: TListView;
    tabStavke: TTabItem;
    listViewStavke: TListView;
    tabEditor: TTabItem;
    lblDokOpis: TLabel;
    lblDokStavka: TLabel;
    lblDokStavkaMat: TLabel;
    lblDokStavkaMatNaz: TLabel;
    lblDokStavkaUlaz: TLabel;
    lblDokStavkaNabCij: TLabel;
    lblDokStavkaVrnab: TLabel;
    edtUlaz: TEdit;
    edtNabcij: TEdit;
    edtVrnab: TEdit;
    btnSpremi: TButton;
    btnOdustani: TButton;
    listViewPod: TListView;
    BindSourceDBPod: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    BindSourceDBDok: TBindSourceDB;
    LinkListControlToField2: TLinkListControlToField;
    BindSourceStavka: TBindSourceDB;
    LinkListControlToField3: TLinkListControlToField;
    tabSettings: TTabItem;
    lblIpAddress: TText;
    btnSettingsCancel: TButton;
    btnSettingsSpremi: TButton;
    rb127: TRadioButton;
    rb31: TRadioButton;
    rbUnesi: TRadioButton;
    edtIpAddress: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure TitleActionUpdate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TabControl1Change(Sender: TObject);
    procedure edtUlazExit(Sender: TObject);
    procedure edtNabcijExit(Sender: TObject);
    procedure edtVrnabExit(Sender: TObject);
    procedure btnOdustaniClick(Sender: TObject);
    procedure btnSpremiClick(Sender: TObject);
    procedure sbAddPodClick(Sender: TObject);
    procedure listViewPodItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure listViewDokItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure listViewStavkeItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure listViewDokUpdateObjects(const Sender: TObject;
      const AItem: TListViewItem);
    procedure btnSettingsCancelClick(Sender: TObject);
    procedure btnSettingsSpremiClick(Sender: TObject);
  private
    FPods: TListaPoduzeca;
    FDoks: TListaDokumenata;
    FStavke: TListaStavki;
    FPod: Integer;
    FDokId: Integer;
    { Private declarations }
    function GetPodData: IPoduzeceData;
    function GetDokData: IDokumentData;
    function GetStavkeData: IStavkeData;
    procedure RefreshPodListView;
    procedure RefreshDokListView(APod: Integer);
    procedure RefreshDokStavkeListView(ADokId: Integer);
    procedure IzbrisiPoduzeca;
    procedure NapuniPoduzeca;
    procedure IzbrisiDokumente(Pod: Integer);
    procedure NapuniDokumente(Pod: Integer);
    procedure IzbrisiStavke(DokId, Pod: Integer);
    procedure NapuniStavke(DokId, Pod: Integer);
    procedure SettingsClicked;
  public
    { Public declarations }
    property Pods: TListaPoduzeca read FPods write FPods;
    property Doks: TListaDokumenata read FDoks write FDoks;
    property Stavke: TListaStavki read FStavke write FStavke;
    property PropPod: Integer read FPod write FPod;
    property PropDokId: Integer read FDokId write FDokId;
  end;

  // const
  const
    IPPORT = '61116';
    HTTPPROTOCOL = 'http://';
    USERNAME = 'iti';
    OPERATER = 0;
    PASS = 'http://';

var
  PrijemRobeForm: TPrijemRobeForm;
  IPADDRESS: String;



implementation

{$R *.fmx}


uses JSON
  , DateUtils
  , System.UIConsts
  , Rest.Types
  ;

procedure TPrijemRobeForm.TabControl1Change(Sender: TObject);
begin
  if TabControl1.ActiveTab <> nil then
  begin
    ToolbarLabel.Text := TabControl1.ActiveTab.Text;
    if TabControl1.ActiveTab = tabDokumenti then
      RefreshDokListView(PropPod);
    if TabControl1.ActiveTab = tabStavke then
      RefreshDokStavkeListView(PropDokId);
  end else
    ToolbarLabel.Text := '';

end;

procedure TPrijemRobeForm.TitleActionUpdate(Sender: TObject);
begin
  if Sender is TCustomAction then
  begin
    if TabControl1.ActiveTab <> nil then
      TCustomAction(Sender).Text := TabControl1.ActiveTab.Text
    else
      TCustomAction(Sender).Text := '';
  end;
end;

procedure TPrijemRobeForm.btnOdustaniClick(Sender: TObject);
var
  stavka: TStavke;
begin
  stavka := TStavke.Create;
  GetStavkeData.StavkeRead(btnOdustani.Tag, stavka);
  edtUlaz.Text := CurrToStr(stavka.MUl);
  edtNabcij.Text := CurrToStr(stavka.MNabcij);
  edtVrnab.Text := CurrToStr(stavka.MVrnab);
  lblDokOpis.Text := 'Dokument: ' + stavka.DokId.ToString;
  lblDokStavka.Text := 'Stavka: ' + stavka.St.ToString;
  lblDokStavkaMat.Text := '�ifra materijala: ' + stavka.Mat.ToString;
  lblDokStavkaMatNaz.Text := 'Artikal: ' + stavka.Naz;
  stavka.Free;
end;

procedure TPrijemRobeForm.btnSettingsCancelClick(Sender: TObject);
begin
  LoadIPAddress(IPADDRESS);
  edtIpAddress.Text := IPADDRESS;
  SettingsClicked;
  TabControl1.ActiveTab := tabPoduzece;
end;

procedure TPrijemRobeForm.btnSettingsSpremiClick(Sender: TObject);
begin
  if rb127.IsChecked then
    IPADDRESS := '127.0.0.1';
  if rb31.IsChecked then
    IPADDRESS := '31.45.234.138';
  if rbUnesi.IsChecked then
    IPADDRESS := Trim(edtIpAddress.Text);
  SaveIPAddress(IPADDRESS);
  SettingsClicked;
  TabControl1.ActiveTab := tabPoduzece;
end;

procedure TPrijemRobeForm.btnSpremiClick(Sender: TObject);
var
  stavka: TStavke;
  dokument: TDokument;
begin
  stavka := TStavke.Create;
  dokument := TDokument.Create;
  GetStavkeData.StavkeRead(btnOdustani.Tag, stavka);
  GetDokData.DokumentRead(stavka.DokId, dokument);
  stavka.MUl := StrToCurr(Trim(edtUlaz.Text));
  stavka.MNabcij := StrToCurr(Trim(edtNabcij.Text));
  stavka.MVrnab := StrToCurr(Trim(edtVrnab.Text));
  stavka.Status := 1;
  GetStavkeData.StavkeUpdate(stavka);
  dokument.Status := 8;
  GetDokData.DokumentUpdate(dokument);
  dokument.Free;
  stavka.Free;
end;

procedure TPrijemRobeForm.edtNabcijExit(Sender: TObject);
var
  Ul, Nabcij, Vrnab: Currency;
begin
  Ul := StrToCurr(Trim(edtUlaz.Text));
  Nabcij := StrToCurr(Trim(edtNabcij.Text));
  Vrnab := Ul * Nabcij;
  edtVrnab.Text := CurrToStr(Vrnab);
end;

procedure TPrijemRobeForm.edtUlazExit(Sender: TObject);
var
  Ul, Nabcij, Vrnab: Currency;
begin
  Ul := StrToCurr(Trim(edtUlaz.Text));
  Nabcij := StrToCurr(Trim(edtNabcij.Text));
  Vrnab := Ul * Nabcij;
  edtVrnab.Text := CurrToStr(Vrnab);
end;

procedure TPrijemRobeForm.edtVrnabExit(Sender: TObject);
var
  Ul, Nabcij, Vrnab: Currency;
begin
  Ul := StrToCurr(Trim(edtUlaz.Text));
  Nabcij := StrToCurr(Trim(edtNabcij.Text));
  Vrnab := StrToCurr(Trim(edtVrnab.Text));
  if Ul <> 0.0 then
    Nabcij := Vrnab / Ul
  else
    Vrnab := 0.0;

  edtNabcij.Text := CurrToStr(Nabcij);
  edtVrnab.Text := CurrToStr(Vrnab);
end;

procedure TPrijemRobeForm.FormCreate(Sender: TObject);
begin
  { This defines the default active tab at runtime }
  TabControl1.ActiveTab := tabSettings;
  FPods := TListaPoduzeca.Create;
  FDoks := TListaDokumenata.Create;
  FStavke := TListaStavki.Create;
end;

procedure TPrijemRobeForm.FormDestroy(Sender: TObject);
var
  I: Integer;
begin
  for I := Pods.Count - 1 downto 0 do
    Pods[I].Free;
  Pods.Free;
  try
    for I := Doks.Count - 1 downto 0 do
      Doks[I].Free;
    Doks.Free;
  except
    On E:Exception do
      ;
  end;
  for I := Stavke.Count - 1 downto 0 do
    Stavke[I].Free;
  Stavke.Free;
  if dictDokumenti <> nil then
  begin
    dictDokumenti.Clear;
    dictDokumenti.Free;
  end;
end;

procedure TPrijemRobeForm.FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char; Shift: TShiftState);
begin
  if (Key = vkHardwareBack) and (TabControl1.TabIndex <> 0) then
  begin
    TabControl1.First;
    Key := 0;
  end;
end;

procedure TPrijemRobeForm.FormShow(Sender: TObject);
begin
  LoadIPAddress(IPADDRESS);
  edtIpAddress.Text := IPADDRESS;
end;

function TPrijemRobeForm.GetDokData: IDokumentData;
begin
  if DMGlavni = nil then
    DMGlavni := TDMGlavni.Create(Application);
  Result := DMGlavni;
end;

function TPrijemRobeForm.GetPodData: IPoduzeceData;
begin
  if DMGlavni = nil then
    DMGlavni := TDMGlavni.Create(Application);
  Result := DMGlavni;
end;

function TPrijemRobeForm.GetStavkeData: IStavkeData;
begin
  if DMGlavni = nil then
    DMGlavni := TDMGlavni.Create(Application);
  Result := DMGlavni;
end;

procedure TPrijemRobeForm.IzbrisiDokumente(Pod: Integer);
var
  I: Integer;
begin
  if Doks.Count = 0 then Exit;

  for I := Doks.Count - 1 downto 0 do
  begin
    if Doks[I].Status <> 8 then
    begin
      dictDokumenti.Remove(Doks[I].God.ToString + '/' +
                            Doks[I].Pod.ToString + '/' +
                            Doks[I].Org.ToString + '/' +
                            Doks[I].Dok.ToString + '/' +
                            Doks[I].Br.ToString);
      DMGlavni.DokumentDelete(Doks[I].Id);
      Doks[I].Free;
    end;
  end;
end;

procedure TPrijemRobeForm.IzbrisiPoduzeca;
var
  I: Integer;
begin
  for I := Pods.Count - 1 downto 0 do
  begin
    DMGlavni.PoduzeceDelete(Pods[I].Id);
    Pods[I].Free;
  end;
  Pods.Clear;
end;

procedure TPrijemRobeForm.IzbrisiStavke(DokId, Pod: Integer);
var
  I: Integer;
begin
  for I := Stavke.Count - 1 downto 0 do
  begin
    DMGlavni.StavkeDelete(Stavke[I].Id);
    Stavke[I].Free;
  end;
  Stavke.Clear;

end;

procedure TPrijemRobeForm.listViewDokItemClick(const Sender: TObject;
  const AItem: TListViewItem);
var
  selItem: Integer;
  AGod, APod, AOrg, ADok, ABr, ADokId, AStatus: Integer;
  Dok: TDokument;
begin
  selItem := listViewDok.Selected.Index;
  ADokId := listViewDok.Items[selItem].Tag;
  ADokId := DMGlavni.FDListViewDokID.AsInteger;
  if DebugOn then ShowMessage(DMGlavni.FDListViewDokDOKUMENT.AsString + ' Id: ' +
    ADokId.ToString);

  Dok := TDokument.Create;
  GetDokData.DokumentRead(ADokId, Dok);

  AGod := Dok.God;
  APod := Dok.Pod;
  AOrg := Dok.Org;
  ADok := Dok.Dok;
  ABr := Dok.Br;
  AStatus := Dok.Status;
  Dok.Free;

  GetStavkeData.StavkeList(ADokId, FStavke);
  if AStatus <> 8 then
  begin
    DMGlavni.RESTClientStavke.BaseURL := HTTPPROTOCOL + IPADDRESS + ':' + IPPORT + '/datasnap/rest/tsm/getdokstavke/' + AGod.ToString + '/' +
      APod.ToString + '/' + AOrg.ToString + '/' + ADok.ToString + '/' + ABr.ToString;
    DMGlavni.RESTRequestStavke.Execute;
    // we got proper response from server
    if DMGlavni.RESTRequestStavke.Response.StatusCode = 200 then
    begin
      NapuniStavke(ADokId, APod);
    end;
  end;
  // RefreshDokStavkeListView(ADokId);
  PropDokId := ADokId;
  TabControl1.ActiveTab := tabStavke;
  ToolBarLabel.Text := ToolBarLabel.Text + ' ' + AGod.ToString + '/' + APod.ToString + '/' + AOrg.ToString + '/' + ADok.ToString + '/' + ABr.ToString;
end;

procedure TPrijemRobeForm.listViewDokUpdateObjects(const Sender: TObject;
  const AItem: TListViewItem);
var
  TxtField: TListItemText;
begin
  TxtField := TListItemText(AItem.View.FindDrawable(TListViewItem.TObjectNames.Text));
  if TxtField = nil then Exit;

  if DMGlavni.FDListViewDokStatus.AsInteger = 6 then
    TxtField.TextColor := TAlphaColorRec.Green
  else
    TxtField.TextColor := TAlphaColorRec.Red;
end;

procedure TPrijemRobeForm.listViewPodItemClick(const Sender: TObject;
  const AItem: TListViewItem);
var
  selItem: Integer;
  nPod: Integer;
begin
  selItem := listViewPod.Selected.Index;
  // nPod := (listViewPod.Items[selItem].Objects.FindObjectT<TListItemText>('Pod').Text).ToInteger;
  nPod := DMGlavni.FDListViewPod.FieldByName('pod').AsInteger;
  PropPod := nPod;
  GetDokData.DokumentList(FDoks);

  DMGlavni.RESTClientDoks.BaseURL := HTTPPROTOCOL + IPADDRESS + ':' + IPPORT + '/datasnap/rest/tsm/getdoks/' + YearOf(Date).ToString + '/'+ nPod.ToString;
  DMGlavni.RESTRequestDoks.Execute;
  // we got proper response from server
  if DMGlavni.RESTRequestDoks.Response.StatusCode = 200 then
  begin
    // napuni dok tablicu
    NapuniDokumente(PropPod);
  end;

  // u�itaj podatke iz dok tablice
  // RefreshDokListView(nPod);
  TabControl1.ActiveTab := tabDokumenti;
end;

procedure TPrijemRobeForm.listViewStavkeItemClick(const Sender: TObject;
  const AItem: TListViewItem);
var
  stavkaId, selItem: Integer;
  stavka: TStavke;
begin
  selItem := listViewStavke.Selected.Index;
  if DebugOn then ShowMessage('Odabrana Stavka : ' + DMGlavni.FDListViewStavkaST.AsString);
  stavkaId := DMGlavni.FDListViewStavkaID.AsInteger;
  // napuni vrijednosti iz stake u edit formu

  stavka := TStavke.Create;
  GetStavkeData.StavkeRead(stavkaId, stavka);
  edtUlaz.Text := CurrToStr(stavka.MUl);
  edtNabcij.Text := CurrToStr(stavka.MNabcij);
  edtVrnab.Text := CurrToStr(stavka.MVrnab);
  lblDokOpis.Text := 'Dokument: ' + stavka.DokId.ToString;
  lblDokStavka.Text := 'Stavka: ' + stavka.St.ToString;
  lblDokStavkaMat.Text := '�ifra materijala: ' + stavka.Mat.ToString;
  lblDokStavkaMatNaz.Text := 'Artikal: ' + stavka.Naz;
  btnSpremi.Tag := stavkaId;
  btnOdustani.Tag := stavkaId;
  stavka.Free;

  TabControl1.ActiveTab := tabEditor;

end;

procedure TPrijemRobeForm.NapuniDokumente(Pod: Integer);
var
  LResult: TJSONArray;
  LJsonResponse: TJSONObject;
  jv: TJSONValue;
  Dok: TDokument;
  RetVal: Integer;
  ValuePair: String;
  fmt: TFormatSettings;
begin

  GetLocaleFormatSettings(0, fmt);

  fmt.ShortDateFormat := 'dd.mm.yyyy';
  fmt.DateSeparator := '.';

  IzbrisiDokumente(Pod);
  LJsonResponse := TJSONObject.ParseJSONValue(DMGlavni.RESTResponseDoks.Content) as TJSONObject;
  LResult := LJsonResponse.GetValue('result') as TJSONArray;
  jv := LResult.Items[0];
  LResult := jv.GetValue<TJSONArray>('Dokumenti');

  try
    ValuePair := '';
    // if LResult[0].GetValue<String>('GetDoks empty') = 'fdqryDok parse all ' then Exit
    // if LResult[0].TryGetValue('GetDoks empty', jv) then
    jv := LResult.Items[0];
    ValuePair := jv.GetValue<String>('GetDoks empty');
    if ValuePair <> '' then Exit;
  except
    ;
  end;

  for jv in LResult do begin
    Dok := TDokument.Create;
    try
      Dok.God := jv.GetValue<Integer>('god');
      Dok.Pod := jv.GetValue<Integer>('pod');
      Dok.Org := jv.GetValue<Integer>('org');
      Dok.Dok := jv.GetValue<Integer>('dok');
      Dok.Br := jv.GetValue<Integer>('br');
      Dok.Par := jv.GetValue<Integer>('par');
      Dok.Datk := StrToDate(jv.GetValue<String>('datk'), fmt);
      Dok.Status := jv.GetValue<Integer>('status');
      Dok.Oper := jv.GetValue<Integer>('oper');
      Dok.Vrijeme := StrToDateTime(jv.GetValue<String>('vrijeme'), fmt);

      if not dictDokumenti.ContainsKey(Dok.God.ToString + '/' +
                                Dok.Pod.ToString + '/' +
                                Dok.Org.ToString + '/' +
                                Dok.Dok.ToString + '/' +
                                Dok.Br.ToString) then
      begin

        RetVal := DMGlavni.DokumentCreate(Dok);

        // TODO da li smo sve napunili? �to ako je gre�ka?
        if RetVal > -1 then
        begin
          Dok.Id := RetVal;
          Doks.Add(Dok);
        end;
      end;
    except
      On E:Exception do
        ShowMessage('NapuniDokumente: ' + E.Message);
    end;
  end;

end;

procedure TPrijemRobeForm.NapuniPoduzeca;
var
  LResult: TJSONArray;
  LJsonResponse: TJSONObject;
  jv: TJSONValue;
  Pod: TPoduzece;
begin
  IzbrisiPoduzeca;
  LJsonResponse := TJSONObject.ParseJSONValue(DMGlavni.RESTResponsePods.Content) as TJSONObject;
  LResult := LJsonResponse.GetValue('result') as TJSONArray;
  jv := LResult.Items[0];
  LResult := jv.GetValue<TJSONArray>('Poduzece');
  for jv in LResult do begin
    Pod := TPoduzece.Create;
    try
      Pod.Pod := jv.GetValue<Integer>('pod');
      Pod.Naziv := jv.GetValue<string>('naz');
      DMGlavni.PoduzeceCreate(Pod);
      Pods.Add(Pod);
    except
      On E:Exception do
        ShowMessage('NapuniPoduzeca: ' + E.Message);
    end;
  end;
end;

procedure TPrijemRobeForm.NapuniStavke(DokId, Pod: Integer);
var
  LResult: TJSONArray;
  LJsonResponse: TJSONObject;
  jv: TJSONValue;
  Stavka: TStavke;
  RetVal: Integer;
  ValuePair: String;
  fmt: TFormatSettings;
begin

  GetLocaleFormatSettings(0, fmt);

  fmt.ShortDateFormat := 'dd.mm.yyyy';
  fmt.DateSeparator := '.';
  fmt.DecimalSeparator := ',';

  IzbrisiStavke(DokId, Pod);
  LJsonResponse := TJSONObject.ParseJSONValue(DMGlavni.RESTResponseStavke.Content) as TJSONObject;
  LResult := LJsonResponse.GetValue('result') as TJSONArray;
  jv := LResult.Items[0];
  LResult := jv.GetValue<TJSONArray>('Stavke');

  try
    ValuePair := '';
    // if LResult[0].TryGetValue('GetDokStavke empty', jv) then
    jv := LResult.Items[0];
    ValuePair := jv.GetValue<String>('GetDokStavke empty');
    if ValuePair <> '' then Exit;
  except
    ;
  end;

  for jv in LResult do begin
    Stavka := TStavke.Create;
    try
      Stavka.DokId := DokId;
      Stavka.Pod := Pod;
      Stavka.St := jv.GetValue<Integer>('st');
      Stavka.Ul := StrToCurr(jv.GetValue<String>('ul'), fmt);
      Stavka.Nabcij := StrToCurr(jv.GetValue<String>('nabcij'), fmt);
      Stavka.Vrnab := StrToCurr(jv.GetValue<String>('vrnab'), fmt);
      Stavka.MUl := StrToCurr(jv.GetValue<String>('ul'), fmt);
      Stavka.MNabcij := StrToCurr(jv.GetValue<String>('nabcij'), fmt);
      Stavka.MVrnab := StrToCurr(jv.GetValue<String>('vrnab'), fmt);
      Stavka.Mat := jv.GetValue<Integer>('mat');
      Stavka.Vmj := jv.GetValue<String>('vmj');
      Stavka.Naz := jv.GetValue<String>('naz');
      Stavka.Napomena := jv.GetValue<String>('napomena');
      Stavka.BarCode := jv.GetValue<String>('bar');
      Stavka.Oper := jv.GetValue<Integer>('operater');
      Stavka.Vrijeme := StrToDateTime(jv.GetValue<String>('vrijeme'), fmt);
      RetVal := DMGlavni.StavkeCreate(Stavka);

      // TODO rije�i gre�ke kod unosa stavke
      if RetVal > -1 then begin
        Stavka.Id := RetVal;
        Stavke.Add(Stavka);
      end;
    except
      On E:Exception do
        ShowMessage('NapuniStavke: ' + E.Message);
    end;
  end;

end;

procedure TPrijemRobeForm.SettingsClicked;
begin
  // https://stackoverflow.com/questions/45940861/android-8-cleartext-http-traffic-not-permitted
  try
    GetPodData.PoduzeceList(FPods);
    DMGlavni.RESTClientPods.BaseURL := HTTPPROTOCOL + IPADDRESS + ':' + IPPORT + '/datasnap/rest/tsm/getpods';
    DMGlavni.RESTRequestPods.Execute;
    // we got proper response from server
    if DMGlavni.RESTRequestPods.Response.StatusCode = 200 then
    begin
      // napuni pod tablicu
      NapuniPoduzeca;
    end;
    // u�itaj podatke iz pod tablice
    RefreshPodListView;
  finally
  end;
end;

procedure TPrijemRobeForm.RefreshDokListView(APod: Integer);
var
  Dok: TDokument;
  Item: TListViewItem;
begin
  // GetDokData.DokumentList(Doks);
  // listViewDok.BeginUpdate;
  LinkListControlToField2.Active := false;
  try
    listViewDok.Items.Clear;
    // for Dok in Doks do
    begin
      // Item := listViewDok.Items.Add;
      // Item.Tag := Dok.Id;
      // Item.Objects.FindObjectT<TListItemText>('Text1').Text := Dok.God.ToString + '/' +
      //        Dok.Pod.ToString + '/' +
      //        Dok.Org.ToString + '/' +
      //        Dok.Dok.ToString + '/' +
      //        Dok.Br.ToString + ' Par : ' +
      //        Dok.Par.ToString + ' na ' +
      //        DateToStr(Dok.Datk);
      //      if Dok.Status <> 8 then
      //        Item.Objects.FindObjectT<TListItemText>('Text1').TextColor := claBlack
      //      else
      //        Item.Objects.FindObjectT<TListItemText>('Text1').TextColor := claRed;
      if DMGlavni.FDListViewDok.Active then
        DMGlavni.FDListViewDok.Close;
      DMGlavni.FDListViewDok.Params.ParamByName('pod').Value := APod;
      DMGlavni.FDListViewDok.Open;

    end;
  finally
    // listViewDok.EndUpdate;
    LinkListControlToField2.Active := true;
  end;
end;

procedure TPrijemRobeForm.RefreshDokStavkeListView(ADokId: Integer);
//var
//  Item: TListViewItem;
//  Stavka: TStavke;
begin
//  GetStavkeData.StavkeList(ADokId, Stavke);
//  listViewStavke.BeginUpdate;
//  try
//    listViewStavke.Items.Clear;
//    for Stavka in Stavke do
//    begin
//      Item := listViewStavke.Items.Add;
//      Item.Tag := Stavka.Id;
//      Item.Objects.FindObjectT<TListItemText>('Stavka').Text := Stavka.St.ToString;
//      Item.Objects.FindObjectT<TListItemText>('Mat').Text := Stavka.Mat.ToString;
//      Item.Objects.FindObjectT<TListItemText>('Naz').Text := Stavka.Naz;
//      Item.Objects.FindObjectT<TListItemText>('Ul').Text := CurrToStr(Stavka.Ul);
//
//    end;
//  finally
//    listViewDok.EndUpdate;
//  end;
  if DMGlavni.FDListViewStavka.Active then
    DMGlavni.FDListViewStavka.Close;
  DMGlavni.FDListViewStavka.Params.ParamByName('DokId').Value := ADokId;
  DMGlavni.FDListViewStavka.Open;

end;

procedure TPrijemRobeForm.RefreshPodListView;
var
  Pod: TPoduzece;
  Item: TListViewItem;
begin
//  GetPodData.PoduzeceList(Pods);
//  listViewPod.BeginUpdate;
  try
//    listboxPod.Items.Clear;
    try
      if not DMGlavni.FDListViewPod.Active then
        DMGlavni.FDListViewPod.Open
      else
        DMGlavni.FDListViewPod.Refresh;
    except
      On E:Exception do
        ;
    end;
    // listViewPod.Items.Clear;
//    for Pod in Pods do
//    begin
//      Item := listViewPod.Items.Add;
//      Item.Tag := Pod.Id;
//      Item.Objects.FindObjectT<TListItemText>('Pod').Text := Pod.Pod.ToString;
//      Item.Objects.FindObjectT<TListItemText>('Naziv').Text := Pod.Naziv;
//
//    end;

  finally
//    listViewPod.EndUpdate;
  end;
end;

procedure TPrijemRobeForm.sbAddPodClick(Sender: TObject);
var
  fJsonObject, fJsonStavka: TJsonObject;
  fJsonResponse: TJSONObject;
  fJsonStavke: TJSONArray;
  LResultPair: TJSONPair;
  jv: TJSONValue;
  strContent, k, v: String;
  Dokum: TDokument;
  Stavka: TStavke;
  fmt: TFormatSettings;
begin

  GetLocaleFormatSettings(0, fmt);

  fmt.ShortDateFormat := 'dd.mm.yyyy';
  fmt.DateSeparator := '.';
  fmt.DecimalSeparator := ',';

  DMGlavni.RESTRequestSetDokStavke.Method := rmPost;
  try
    for Dokum in Doks do
    begin
      if Dokum.Status = 8 then
      begin
        // dohvati stavke dokumenta
        GetStavkeData.StavkeList(Dokum.Id, Stavke);
        DMGlavni.RESTClientSetDokStavke.BaseURL := HTTPPROTOCOL + IPADDRESS + ':' + IPPORT + '/datasnap/rest/tsm/setdokstavke/' +
          Dokum.God.ToString + '/' +
          Dokum.Pod.ToString + '/' +
          Dokum.Org.ToString + '/' +
          Dokum.Dok.ToString + '/' +
          Dokum.Br.ToString;
        fJsonObject := TJSONObject.Create;
        fJsonObject.AddPair('God', Dokum.God.ToString);
        fJsonObject.AddPair('Pod', Dokum.Pod.ToString);
        fJsonObject.AddPair('Org', Dokum.Org.ToString);
        fJsonObject.AddPair('Dok', Dokum.Dok.ToString);
        fJsonObject.AddPair('Br', Dokum.Br.ToString);
        fJsonObject.AddPair('DokId', Dokum.Id.ToString);
        fJsonObject.AddPair('Par', Dokum.Par.ToString);
        fJsonObject.AddPair('Datk', DateToStr(Dokum.Datk, fmt));
        fJsonObject.AddPair('Status', Dokum.Status.ToString);
        fJsonObject.AddPair('Oper', Dokum.Oper.ToString);
        fJsonObject.AddPair('Vrijeme', DateTimeToStr(Dokum.Vrijeme, fmt));
        fJsonStavke := TJsonArray.Create;
        for Stavka in Stavke do
        begin
          // napravi json sa stavkama
          fJsonStavka := TJsonObject.Create;
          fJsonStavka.AddPair('St', Stavka.St.ToString);
          fJsonStavka.AddPair('StavkaId', Stavka.Id.ToString);
          fJsonStavka.AddPair('Ul', CurrToStr(Stavka.Ul, fmt));
          fJsonStavka.AddPair('Nabcij', CurrToStr(Stavka.Nabcij, fmt));
          fJsonStavka.AddPair('Vrnab', CurrToStr(Stavka.Vrnab, fmt));
          fJsonStavka.AddPair('MUl', CurrToStr(Stavka.MUl, fmt));
          fJsonStavka.AddPair('MNabcij', CurrToStr(Stavka.MNabcij, fmt));
          fJsonStavka.AddPair('MVrnab', CurrToStr(Stavka.MVrnab, fmt));
          fJsonStavka.AddPair('Mat', Stavka.Mat.ToString);
          fJsonStavka.AddPair('Naz', Stavka.Naz);
          fJsonStavka.AddPair('Napomena', Stavka.Napomena);
          fJsonStavka.AddPair('BarCode', Stavka.BarCode);
          fJsonStavka.AddPair('Vmj', Stavka.Vmj);
          fJsonStavka.AddPair('Status', Stavka.Status.ToString);
          fJsonStavka.AddPair('Oper', Stavka.Oper.ToString);
          fJsonStavka.AddPair('Vrijeme', DateTimeToStr(Stavka.Vrijeme, fmt));
          fJsonStavke.Add(fJsonStavka);
        end;

        fJsonObject.AddPair('Stavka', fJsonStavke);
        // po�alji na Datasnap server
        DMGlavni.RESTRequestSetDokStavke.Body.Add(fJsonObject);
        try
          DMGlavni.RESTRequestSetDokStavke.Execute;
          if DMGlavni.RESTRequestSetDokStavke.Response.StatusCode = 200 then
          begin
            if DebugOn then ShowMessage('Sync OK ' + DMGlavni.RESTResponseSetDokStavke.Content);
            Dokum.Status := 6;
            GetDokData.DokumentUpdate(Dokum);
          end
          else
            if DebugOn then ShowMessage('Sync failed, returned status code ' + DMGlavni.RESTRequestSetDokStavke.Response.StatusCode.ToString);
        except
          On E:Exception do
            if DebugOn then ShowMessage('Sync exception ' + E.Message + chr(13) + chr(10) + DMGlavni.RESTClientSetDokStavke.BaseURL);
        end;
      end;
    end;
  finally
    GetDokData.DokumentList(Doks);
  end;

//  try
//
//    fJsonObject := TJSONObject.Create;
//    fJsonObject.AddPair('Orders','{"key":"value"}');
//    DMGlavni.RESTRequestSetDokStavke.Body.Add(fJsonObject);
//    DMGlavni.RESTRequestSetDokStavke.Method := rmPost;
//    try
//      DMGlavni.RESTRequestSetDokStavke.Execute;
//      // we got proper response from server
//      if DMGlavni.RESTRequestSetDokStavke.Response.StatusCode = 200 then
//      begin
//        // napuni dok tablicu
//        // ShowMessage('Plus clicked returned 200: ' + DMGlavni.RESTRequestSetDokStavke.Response.JSONText);
//        fJsonResponse := TJSONObject.ParseJSONValue(DMGlavni.RESTResponseSetDokStavke.Content) as TJSONObject;
//        LResult := fJsonResponse.GetValue('result') as TJSONArray;
//        strContent := LResult.ToString;
//        jv := LResult.Items[0];
//        strContent := jv.ToString;
//        jv := jv.GetValue<TJsonValue>('UpdateSetDokStavke');
//        strContent := jv.ToString;
//        strContent := StringReplace(strContent, '\', '', [rfReplaceAll]);
//        strContent := StringReplace(strContent, '"{', '{', [rfReplaceAll]);
//        strContent := StringReplace(strContent, '}"', '}', [rfReplaceAll]);
//        jv := TJsonObject.ParseJSONValue(strContent, False, True);
//        strContent := jv.ToString;
//        jv := jv.GetValue<TJsonValue>('Orders');
//        strContent := jv.ToString;
//
//        k := 'key';
//        v := jv.GetValue<TJsonValue>('key').ToString;
//        ShowMessage('Plus clicked returned json: key ' + k + ' value ' + v);
//      end else
//        ShowMessage('Plus clicked returned ' + DMGlavni.RESTRequestSetDokStavke.Response.StatusCode.ToString);
//    except
//      On E:Exception do
//        ShowMessage('Plus clicked exception> ' + E.Message + chr(13) + chr(10) + DMGlavni.RESTClientSetDokStavke.BaseURL + chr(13) + chr(10) + strContent);
//    end;
//  finally
//    fJsonObject.Free;
//  end;
end;

end.
