program TestGUI;

uses
  System.StartUpCopy,
  FMX.Forms,
  uTestGUI in 'uTestGUI.pas' {FrmTestGUI},
  DataModule in 'DataModule.pas' {DMGlavni: TDataModule},
  untObjekti in 'untObjekti.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFrmTestGUI, FrmTestGUI);
  Application.CreateForm(TDMGlavni, DMGlavni);
  Application.Run;
end.
