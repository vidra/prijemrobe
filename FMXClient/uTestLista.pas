unit uTestLista;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs
  , DataModule, FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, System.Rtti, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.EngExt, Fmx.Bind.DBEngExt, Data.Bind.Components,
  Data.Bind.DBScope, FMX.ListView, FMX.Objects, FMX.Controls.Presentation,
  FMX.StdCtrls
  ;

type
  TForm1 = class(TForm)
    ListView1: TListView;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkListControlToField1: TLinkListControlToField;
    Text1: TText;
    Text2: TText;
    btnRefresh: TButton;
    procedure FormShow(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure ListView1ItemClick(const Sender: TObject;
      const AItem: TListViewItem);
    procedure btnRefreshClick(Sender: TObject);
  private
    procedure SetTexts;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.JSON
  , untObjekti
  ;

procedure TForm1.btnRefreshClick(Sender: TObject);
var
  LResult: TJSONArray;
  LJsonResponse: TJSONObject;
  jv: TJSONValue;
  Pod: TPoduzece;
  Pods: TListaPoduzeca;
  I: Integer;
begin
  Pods := TListaPoduzeca.Create;
  DMGlavni.PoduzeceList(Pods);
  DMGlavni.RESTRequestPods.Execute;
  // we got proper response from server
  if DMGlavni.RESTRequestPods.Response.StatusCode = 200 then
  begin
    for I := Pods.Count - 1 downto 0 do
    begin
      DMGlavni.PoduzeceDelete(Pods[I].Id);
      Pods[I].Free;
    end;
    Pods.Clear;
    Pods.Free;
    LJsonResponse := TJSONObject.ParseJSONValue(DMGlavni.RESTResponsePods.Content) as TJSONObject;
    LResult := LJsonResponse.GetValue('result') as TJSONArray;
    jv := LResult.Items[0];
    LResult := jv.GetValue<TJSONArray>('Poduzece');
    for jv in LResult do begin
      Pod := TPoduzece.Create;
      try
        Pod.Pod := jv.GetValue<Integer>('pod');
        Pod.Naziv := jv.GetValue<string>('naz');
        DMGlavni.PoduzeceCreate(Pod);
      except
        On E:Exception do
          ShowMessage('NapuniPoduzeca: ' + E.Message);
      end;
    end;
  end;
  DMGlavni.FDListViewPod.Refresh;
  SetTexts;
end;

procedure TForm1.FormShow(Sender: TObject);
var
  Pod: TPoduzece;
begin
  DMGlavni.FDListViewPod.Active := True;
  if DMGlavni.FDListViewPod.IsEmpty then
  begin
    Pod := TPoduzece.Create;
    try
      Pod.Pod := 1;
      Pod.Naziv := 'ITI Trgovina Ru�no';
      DMGlavni.PoduzeceCreate(Pod);
      Pod.Pod := 2;
      Pod.Naziv := 'ITI Hotel Ru�no';
      DMGlavni.PoduzeceCreate(Pod);
      DMGlavni.FDListViewPod.Refresh;
    finally
      Pod.Free;
    end;
  end;
  SetTexts;
end;

procedure TForm1.ListView1DblClick(Sender: TObject);
begin
  // SetTexts;
end;

procedure TForm1.ListView1ItemClick(const Sender: TObject;
  const AItem: TListViewItem);
begin
  SetTexts;
end;

procedure TForm1.SetTexts;
begin
  Text1.Text := DMGlavni.FDListViewPod.FieldByName('pod').AsString;
  Text2.Text := DMGlavni.FDListViewPod.FieldByName('Naziv').AsString;
end;

end.
