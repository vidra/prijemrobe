program PrijemRobe;

uses
  System.StartUpCopy,
  FMX.Forms,
  FormPrijemRobe in 'FormPrijemRobe.pas' {PrijemRobeForm},
  DataModule in 'DataModule.pas' {DMGlavni: TDataModule},
  untObjekti in 'untObjekti.pas',
  uSettings in 'uSettings.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TPrijemRobeForm, PrijemRobeForm);
  Application.CreateForm(TDMGlavni, DMGlavni);
  Application.Run;
end.
