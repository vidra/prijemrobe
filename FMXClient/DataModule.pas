unit DataModule;

interface

uses
  System.SysUtils, System.Classes, REST.Types, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, REST.Response.Adapter, REST.Client, Data.Bind.Components,
  Data.Bind.ObjectScope
  , untObjekti
  , System.Generics.Collections, FireDAC.UI.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  // FireDAC.Phys.SQLiteWrapper.Stat,
  FireDAC.FMXUI.Wait, FireDAC.DApt, FireDAC.Phys.SQLiteWrapper.Stat
  ;

type
  TDMGlavni = class(TDataModule, IPoduzeceData, IDokumentData, IStavkeData)
    RESTClientPods: TRESTClient;
    RESTRequestPods: TRESTRequest;
    RESTResponsePods: TRESTResponse;
    RESTResponsePodsDataSetAdapter: TRESTResponseDataSetAdapter;
    FDMemTablePods: TFDMemTable;
    FDMemTablePodspod: TIntegerField;
    FDMemTablePodsnaz: TStringField;
    fdConsDiventa: TFDConnection;
    fdqPodMaxId: TFDQuery;
    fdqPodUpdate: TFDQuery;
    fdqPodInsert: TFDQuery;
    fdqPodDelete: TFDQuery;
    fdqPodSelect: TFDQuery;
    fdqPodSelectAll: TFDQuery;
    RESTClientDoks: TRESTClient;
    RESTRequestDoks: TRESTRequest;
    RESTResponseDoks: TRESTResponse;
    RESTResponseDoksDataSetAdapter: TRESTResponseDataSetAdapter;
    FDMemTableDoks: TFDMemTable;
    fdqDokMaxId: TFDQuery;
    fdqDokInsert: TFDQuery;
    fdqDokSelect: TFDQuery;
    fdqDokUpdate: TFDQuery;
    fdqDokDelete: TFDQuery;
    fdqDokSelectAll: TFDQuery;
    fdqStavkeMaxId: TFDQuery;
    fdqStavkeInsert: TFDQuery;
    fdqStavkeSelect: TFDQuery;
    fdqStavkeUpdate: TFDQuery;
    fdqStavkeDelete: TFDQuery;
    fdqStavkeSelectAll: TFDQuery;
    RESTClientStavke: TRESTClient;
    RESTRequestStavke: TRESTRequest;
    RESTResponseStavke: TRESTResponse;
    RESTClientSetDokStavke: TRESTClient;
    RESTRequestSetDokStavke: TRESTRequest;
    RESTResponseSetDokStavke: TRESTResponse;
    FDListViewPod: TFDQuery;
    dsListViewPod: TDataSource;
    FDListViewPodId: TIntegerField;
    FDListViewPodPod: TIntegerField;
    FDListViewPodNaziv: TWideMemoField;
    FDListViewDok: TFDQuery;
    FDListViewDokId: TIntegerField;
    FDListViewDokGod: TIntegerField;
    FDListViewDokPod: TIntegerField;
    FDListViewDokORG: TIntegerField;
    FDListViewDokDok: TIntegerField;
    FDListViewDokBr: TIntegerField;
    FDListViewDokPar: TIntegerField;
    FDListViewDokDatk: TDateField;
    FDListViewDokStatus: TIntegerField;
    FDListViewDokOper: TIntegerField;
    FDListViewDokVrijeme: TDateTimeField;
    FDListViewDokDokument: TWideStringField;
    FDListViewStavka: TFDQuery;
    FDListViewStavkaId: TIntegerField;
    FDListViewStavkaDokId: TIntegerField;
    FDListViewStavkaPod: TIntegerField;
    FDListViewStavkaSt: TIntegerField;
    FDListViewStavkaUl: TFloatField;
    FDListViewStavkaNabcij: TFloatField;
    FDListViewStavkaVrnab: TFloatField;
    FDListViewStavkaMat: TIntegerField;
    FDListViewStavkaVmj: TWideMemoField;
    FDListViewStavkaNaz: TWideMemoField;
    FDListViewStavkaNapomena: TWideMemoField;
    FDListViewStavkaBarCode: TWideMemoField;
    FDListViewStavkaStatus: TIntegerField;
    FDListViewStavkaOper: TIntegerField;
    FDListViewStavkaVrijeme: TDateTimeField;
    fdqVerzija: TFDQuery;
    procedure fdConsDiventaAfterConnect(Sender: TObject);
    procedure fdConsDiventaBeforeConnect(Sender: TObject);
  private
    { Private declarations }
    function IsMobilePlatform: Boolean;
    function GetNewPodId: Integer;
    function GetNewDokId: Integer;
    function GetNewStavkeId: Integer;
  public
    function PoduzeceCreate(aValue: TPoduzece): Integer;
    function PoduzeceDelete(id: Integer): Boolean;
    procedure PoduzeceList(aList: TList<TPoduzece>);
    function PoduzeceRead(id: Integer; out aValue: TPoduzece): Boolean;
    function PoduzeceUpdate(aValue: TPoduzece): Boolean;
    function DokumentCreate(aValue: TDokument): integer;
    function DokumentRead(id: integer; out aValue: TDokument): boolean;
    function DokumentUpdate(aValue: TDokument): boolean;
    function DokumentDelete(id: integer): boolean;
    procedure DokumentList(aList: TListaDokumenata);
    function StavkeCreate(aValue: TStavke): integer;
    function StavkeRead(id: integer; out aValue: TStavke): boolean;
    function StavkeUpdate(aValue: TStavke): boolean;
    function StavkeDelete(id: integer): boolean;
    procedure StavkeList(ADokId: Integer; aList: TListaStavki);
  end;

var
  DMGlavni: TDMGlavni;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

uses
  System.IOUtils
  ;

{ TDMGlavni }

function TDMGlavni.DokumentCreate(aValue: TDokument): integer;
var
  NewId: Integer;
begin
  NewId := GetNewDokId + 1;
  fdqDokInsert.ParamByName('Id').AsInteger := NewId;
  fdqDokInsert.ParamByName('God').AsInteger := aValue.God;
  fdqDokInsert.ParamByName('Pod').AsInteger := aValue.Pod;
  fdqDokInsert.ParamByName('Org').AsInteger := aValue.Org;
  fdqDokInsert.ParamByName('Dok').AsInteger := aValue.Dok;
  fdqDokInsert.ParamByName('Br').AsInteger := aValue.Br;
  fdqDokInsert.ParamByName('Par').AsInteger := aValue.Par;
  fdqDokInsert.ParamByName('Datk').AsDate := aValue.Datk;
  fdqDokInsert.ParamByName('Status').AsInteger := aValue.Status;
  fdqDokInsert.ParamByName('Oper').AsInteger := aValue.Oper;
  fdqDokInsert.ParamByName('Vrijeme').AsDateTime := aValue.Vrijeme;
  try
    fdqDokInsert.ExecSQL;
    Result := NewId;
  except
    Result := -1;
  end;

end;

function TDMGlavni.DokumentDelete(id: integer): boolean;
begin
  Result := False;
  fdqDokDelete.ParamByName('Id').AsInteger := id;

  try
    fdqDokDelete.ExecSQL;
  finally
    fdqDokDelete.Close;
  end;
end;

procedure TDMGlavni.DokumentList(aList: TListaDokumenata);
var
  aItem: TDokument;
begin
  if aList <> nil then
  begin
    aList.Clear;
    fdqDokSelectAll.Open();
    try
      if dictDokumenti = nil then
        dictDokumenti := TDictionary<String, Integer>.Create;

      dictDokumenti.Clear;
      while not fdqDokSelectAll.EOF do
      begin
        aItem := TDokument.Create;
        aItem.Id := fdqDokSelectAll.FieldByName('Id').AsInteger;
        aItem.God := fdqDokSelectAll.FieldByName('God').AsInteger;
        aItem.Pod := fdqDokSelectAll.FieldByName('Pod').AsInteger;
        aItem.Org := fdqDokSelectAll.FieldByName('Org').AsInteger;
        aItem.Dok := fdqDokSelectAll.FieldByName('Dok').AsInteger;
        aItem.Br := fdqDokSelectAll.FieldByName('Br').AsInteger;
        aItem.Par := fdqDokSelectAll.FieldByName('Par').AsInteger;
        aItem.Datk := fdqDokSelectAll.FieldByName('Datk').AsDateTime;
        aItem.Status := fdqDokSelectAll.FieldByName('Status').AsInteger;
        aItem.Oper := fdqDokSelectAll.FieldByName('Oper').AsInteger;
        aItem.Vrijeme := fdqDokSelectAll.FieldByName('Vrijeme').AsDateTime;
        aList.add(aItem);

        dictDokumenti.Add(aItem.God.ToString + '/' +
                          aItem.Pod.ToString + '/' +
                          aItem.Org.ToString + '/' +
                          aItem.Dok.ToString + '/' +
                          aItem.Br.ToString, aItem.Id);

        fdqDokSelectAll.Next;
      end;
    finally
      fdqDokSelectAll.Close;
    end;
  end;

end;

function TDMGlavni.DokumentRead(id: integer; out aValue: TDokument): boolean;
begin
  Result := False;
  fdqDokSelect.ParamByName('Id').AsInteger := id;
  fdqDokSelect.Open;
  try
    if fdqDokSelect.RecordCount > 0 then
    begin
      aValue.Id := id;
      aValue.God := fdqDokSelect.FieldByName('God').AsInteger;
      aValue.Pod := fdqDokSelect.FieldByName('Pod').AsInteger;
      aValue.Org := fdqDokSelect.FieldByName('Org').AsInteger;
      aValue.Dok := fdqDokSelect.FieldByName('Dok').AsInteger;
      aValue.Br := fdqDokSelect.FieldByName('Br').AsInteger;
      aValue.Par := fdqDokSelect.FieldByName('Par').AsInteger;
      aValue.Datk := fdqDokSelect.FieldByName('Datk').AsDateTime;
      aValue.Status := fdqDokSelect.FieldByName('Status').AsInteger;
      aValue.Oper := fdqDokSelect.FieldByName('Oper').AsInteger;
      aValue.Vrijeme := fdqDokSelect.FieldByName('Vrijeme').AsDateTime;
      Result := True;
    end;
  finally
    fdqDokSelect.Close;
  end;
end;

function TDMGlavni.DokumentUpdate(aValue: TDokument): boolean;
begin
  fdqDokUpdate.ParamByName('Id').AsInteger := aValue.Id;
  fdqDokUpdate.ParamByName('God').AsInteger := aValue.God;
  fdqDokUpdate.ParamByName('Pod').AsInteger := aValue.Pod;
  fdqDokUpdate.ParamByName('Org').AsInteger := aValue.Org;
  fdqDokUpdate.ParamByName('Dok').AsInteger := aValue.Dok;
  fdqDokUpdate.ParamByName('Br').AsInteger := aValue.Br;
  fdqDokUpdate.ParamByName('Par').AsInteger := aValue.Par;
  fdqDokUpdate.ParamByName('Datk').AsDate := aValue.Datk;
  fdqDokUpdate.ParamByName('Status').AsInteger := aValue.Status;
  fdqDokUpdate.ParamByName('Oper').AsInteger := aValue.Oper;
  fdqDokUpdate.ParamByName('Vrijeme').AsDateTime := aValue.Vrijeme;
  try
    fdqDokUpdate.ExecSQL;
    Result := True
  finally
    Result := False;
  end;
end;

procedure TDMGlavni.fdConsDiventaAfterConnect(Sender: TObject);
const
  sCreateTablePodsSQL = 'CREATE TABLE IF NOT EXISTS Pod ('
    + ' Id INTEGER NOT NULL PRIMARY KEY, '
    + ' Pod INTEGER, Naziv TEXT)';
  sCreateTableDoksSQL = 'CREATE TABLE IF NOT EXISTS Dok ('
    + ' Id INTEGER NOT NULL PRIMARY KEY, '
    + ' God INTEGER, Pod INTEGER, ORG INTEGER, Dok INTEGER, Br INTEGER, '
    + ' Par INTEGER, Datk DATE, Status INTEGER, Oper INTEGER, Vrijeme DATETIME '
    + ')';
  sCreateTableStavkeSQL = 'CREATE TABLE IF NOT EXISTS Stavke ('
    + ' Id INTEGER NOT NULL PRIMARY KEY, '
    + ' DokId INTEGER, Pod INTEGER, St INTEGER, Ul REAL, Nabcij REAL, Vrnab REAL, '
    + ' Mat INTEGER, Vmj TEXT, Naz TEXT, Napomena TEXT, BarCode TEXT, '
    + ' Status INTEGER, Oper INTEGER, Vrijeme DATETIME)';

  sCreateTableVerzijaSQL = 'CREATE TABLE IF NOT EXISTS VERZIJA ('
    + ' Id INTEGER NOT NULL PRIMARY KEY, '
    + ' VerzijaId INTEGER, Naz TEXT, Napomena TEXT, Vrijeme DATETIME)';

  aAlterStavkeMul2SQL = 'ALTER TABLE Stavke ADD MUl REAL DEFAULT 0.0';

  aAlterStavkeMNabcij2SQL = 'ALTER TABLE Stavke ADD MNabcij REAL DEFAULT 0.0';

  aAlterStavkeMVrnab2SQL = 'ALTER TABLE Stavke ADD MVrnab REAL DEFAULT 0.0';

  aInsertVerzija2SQL = 'INSERT INTO Verzija (Id, VerzijaId, Naz, Napomena, Vrijeme) VALUES '
    + ' (1, 2, ''Stare vrijednosti stavki dokumenta'', ''Dodana polja Stavke.MUl, Stavke.MNabcij, Stavke.MVrnab'', date(''now''))';
var
  LVerzija: Integer;
  LID: Integer;
begin
  fdConsDiventa.ExecSQL(sCreateTablePodsSQL);

  fdConsDiventa.ExecSQL(sCreateTableDoksSQL);

  fdConsDiventa.ExecSQL(sCreateTableStavkeSQL);

  fdConsDiventa.ExecSQL(sCreateTableVerzijaSQL);

  // pro�itaj verziju

  fdqVerzija.Open;
  try
    if fdqVerzija.IsEmpty then
    begin
      LVerzija := 1;
      LID := 1;
    end else begin
      LVerzija := fdqVerzija.FieldByName('VerzijaId').AsInteger;
      LID := fdqVerzija.FieldByName('Id').AsInteger;
    end;
  finally
    fdqVerzija.Close;
  end;

  if LVerzija < 2 then
  begin
    fdConsDiventa.ExecSQL(aAlterStavkeMul2SQL);
    fdConsDiventa.ExecSQL(aAlterStavkeMNabcij2SQL);
    fdConsDiventa.ExecSQL(aAlterStavkeMVrnab2SQL);

    fdConsDiventa.ExecSQL(aInsertVerzija2SQL);
  end;
end;

procedure TDMGlavni.fdConsDiventaBeforeConnect(Sender: TObject);
begin
  if IsMobilePlatform then
    fdConsDiventa.Params.Values['Database'] := TPath.Combine(TPath.GetDocumentsPath, 'PrijemRobe.db');

end;

function TDMGlavni.GetNewDokId: Integer;
begin
  fdqDokMaxId.Open();
  try
    Result := fdqDokMaxId.FieldByName('MAXID').AsInteger;
  finally
    fdqDokMaxId.Close;
  end;
end;

function TDMGlavni.GetNewPodId: Integer;
begin
  fdqPodMaxId.Open();
  try
    Result := fdqPodMaxId.FieldByName('MAXID').AsInteger;
  finally
    fdqPodMaxId.Close;
  end;
end;

function TDMGlavni.GetNewStavkeId: Integer;
begin
  fdqStavkeMaxId.Open();
  try
    Result := fdqStavkeMaxId.FieldByName('MAXID').AsInteger;
  finally
    fdqStavkeMaxId.Close;
  end;
end;

function TDMGlavni.IsMobilePlatform: Boolean;
begin
  Result := (TOSVersion.Platform = pfAndroid) or (TOSVersion.Platform = pfiOs);
end;

function TDMGlavni.PoduzeceCreate(aValue: TPoduzece): Integer;
var
  NewId: Integer;
begin
  NewId := GetNewPodId + 1;
  fdqPodInsert.ParamByName('Id').AsInteger := NewId;
  fdqPodInsert.ParamByName('Pod').AsInteger := aValue.Pod;
  fdqPodInsert.ParamByName('Naziv').AsString := aValue.Naziv;
  try
    fdqPodInsert.ExecSQL;
    Result := NewId;
  except
    Result := -1;
  end;
end;

function TDMGlavni.PoduzeceDelete(id: Integer): Boolean;
begin
  Result := False;
  fdqPodDelete.ParamByName('Id').AsInteger := id;

  try
    fdqPodDelete.ExecSQL;
  finally
    fdqPodDelete.Close;
  end;
end;

procedure TDMGlavni.PoduzeceList(aList: TList<TPoduzece>);
var
  aItem: TPoduzece;
begin
  if aList <> nil then
  begin
    aList.Clear;
    fdqPodSelectAll.DisableControls;

    fdqPodSelectAll.Open();
    try
      while not fdqPodSelectAll.EOF do
      begin
        aItem := TPoduzece.Create;
        aItem.Id := fdqPodSelectAll.FieldByName('Id').AsInteger;
        aItem.Pod := fdqPodSelectAll.FieldByName('Pod').AsInteger;
        aItem.Naziv := fdqPodSelectAll.FieldByName('Naziv').AsString;
        aList.add(aItem);
        fdqPodSelectAll.Next;
      end;
    finally
      fdqPodSelectAll.Close;
      fdqPodSelectAll.EnableControls;
    end;
  end;
end;

function TDMGlavni.PoduzeceRead(id: Integer; out aValue: TPoduzece): Boolean;
begin
  Result := False;
  fdqPodSelect.ParamByName('Id').AsInteger := id;
  fdqPodSelect.Open;
  try
    if fdqPodSelect.RecordCount > 0 then
    begin
      aValue.Id := id;
      aValue.Pod := fdqPodSelect.FieldByName('Pod').AsInteger;
      aValue.Naziv := fdqPodSelect.FieldByName('Naziv').AsString;
      Result := True;
    end;
  finally
    fdqPodSelect.Close;
  end;

end;

function TDMGlavni.PoduzeceUpdate(aValue: TPoduzece): Boolean;
begin
  fdqPodUpdate.ParamByName('Id').AsInteger := aValue.Id;
  fdqPodUpdate.ParamByName('Pod').AsInteger := aValue.Pod;
  fdqPodUpdate.ParamByName('Naziv').AsString := aValue.Naziv;
  try
    fdqPodUpdate.ExecSQL;
    Result := True
  finally
    Result := False;
  end;
end;

function TDMGlavni.StavkeCreate(aValue: TStavke): integer;
var
  NewId: Integer;
begin
  NewId := GetNewStavkeId + 1;
  fdqStavkeInsert.ParamByName('Id').AsInteger := NewId;
  fdqStavkeInsert.ParamByName('DokId').AsInteger := aValue.DokId;
  fdqStavkeInsert.ParamByName('Pod').AsInteger := aValue.Pod;
  fdqStavkeInsert.ParamByName('St').AsInteger := aValue.St;
  fdqStavkeInsert.ParamByName('Ul').AsCurrency := aValue.Ul;
  fdqStavkeInsert.ParamByName('Nabcij').AsCurrency := aValue.Nabcij;
  fdqStavkeInsert.ParamByName('Vrnab').AsCurrency := aValue.Vrnab;
  fdqStavkeInsert.ParamByName('MUl').AsCurrency := aValue.MUl;
  fdqStavkeInsert.ParamByName('MNabcij').AsCurrency := aValue.MNabcij;
  fdqStavkeInsert.ParamByName('MVrnab').AsCurrency := aValue.MVrnab;
  fdqStavkeInsert.ParamByName('Mat').AsInteger := aValue.Mat;
  fdqStavkeInsert.ParamByName('Vmj').AsString := aValue.Vmj;
  fdqStavkeInsert.ParamByName('Naz').AsString := aValue.Naz;
  fdqStavkeInsert.ParamByName('Napomena').AsString := aValue.Napomena;
  fdqStavkeInsert.ParamByName('BarCode').AsString := aValue.BarCode;
  fdqStavkeInsert.ParamByName('Oper').AsInteger := aValue.Oper;
  fdqStavkeInsert.ParamByName('Vrijeme').AsDateTime := aValue.Vrijeme;
  try
    fdqStavkeInsert.ExecSQL;
    Result := NewId;
  except
    Result := -1;
  end;

end;

function TDMGlavni.StavkeDelete(id: integer): boolean;
begin
  Result := False;
  fdqStavkeDelete.ParamByName('Id').AsInteger := id;

  try
    fdqStavkeDelete.ExecSQL;
  finally
    fdqStavkeDelete.Close;
  end;
end;

procedure TDMGlavni.StavkeList(ADokId: Integer; aList: TListaStavki);
var
  aItem: TStavke;
  I:Integer;
begin
  if aList = nil then raise Exception.Create('StavkeList pozvan sa aList parametrom nil');

  if ADokId < 1 then raise Exception.Create('StavkeList pozvan sa ADokId parametrom ' + ADokId.ToString);

  for I := aList.Count - 1 downto 0 do
    aList[I].Free;

  aList.Clear;

  fdqStavkeSelectAll.Params.ParamByName('DokId').AsInteger := ADokId;
  fdqStavkeSelectAll.Open();
  try
    while not fdqStavkeSelectAll.EOF do
    begin
      aItem := TStavke.Create;
      aItem.Id := fdqStavkeSelectAll.FieldByName('Id').AsInteger;
      aItem.DokId := fdqStavkeSelectAll.FieldByName('DokId').AsInteger;
      aItem.Pod := fdqStavkeSelectAll.FieldByName('Pod').AsInteger;
      aItem.St := fdqStavkeSelectAll.FieldByName('St').AsInteger;
      aItem.Ul := fdqStavkeSelectAll.FieldByName('Ul').AsCurrency;
      aItem.Nabcij := fdqStavkeSelectAll.FieldByName('Nabcij').AsCurrency;
      aItem.Vrnab := fdqStavkeSelectAll.FieldByName('Vrnab').AsCurrency;
      aItem.MUl := fdqStavkeSelectAll.FieldByName('MUl').AsCurrency;
      aItem.MNabcij := fdqStavkeSelectAll.FieldByName('MNabcij').AsCurrency;
      aItem.MVrnab := fdqStavkeSelectAll.FieldByName('MVrnab').AsCurrency;
      aItem.Mat := fdqStavkeSelectAll.FieldByName('Mat').AsInteger;
      aItem.Vmj := fdqStavkeSelectAll.FieldByName('Vmj').AsString;
      aItem.Naz := fdqStavkeSelectAll.FieldByName('Naz').AsString;
      aItem.Napomena := fdqStavkeSelectAll.FieldByName('Napomena').AsString;
      aItem.BarCode := fdqStavkeSelectAll.FieldByName('BarCode').AsString;
      aItem.Oper := fdqStavkeSelectAll.FieldByName('Oper').AsInteger;
      aItem.Vrijeme := fdqStavkeSelectAll.FieldByName('Vrijeme').AsDateTime;
      aList.add(aItem);
      fdqStavkeSelectAll.Next;
    end;
  finally
    fdqStavkeSelectAll.Close;
  end;
end;

function TDMGlavni.StavkeRead(id: integer; out aValue: TStavke): boolean;
begin
  Result := False;
  fdqStavkeSelect.ParamByName('Id').AsInteger := id;
  fdqStavkeSelect.Open;
  try
    if fdqStavkeSelect.RecordCount > 0 then
    begin
      aValue.Id := id;
      aValue.DokId := fdqStavkeSelect.FieldByName('DokId').AsInteger;
      aValue.Pod := fdqStavkeSelect.FieldByName('Pod').AsInteger;
      aValue.St := fdqStavkeSelect.FieldByName('St').AsInteger;
      aValue.Ul := fdqStavkeSelect.FieldByName('Ul').AsCurrency;
      aValue.Nabcij := fdqStavkeSelect.FieldByName('Nabcij').AsCurrency;
      aValue.Vrnab := fdqStavkeSelect.FieldByName('Vrnab').AsCurrency;
      aValue.MUl := fdqStavkeSelect.FieldByName('MUl').AsCurrency;
      aValue.MNabcij := fdqStavkeSelect.FieldByName('MNabcij').AsCurrency;
      aValue.MVrnab := fdqStavkeSelect.FieldByName('MVrnab').AsCurrency;
      aValue.Mat := fdqStavkeSelect.FieldByName('Mat').AsInteger;
      aValue.Vmj := fdqStavkeSelect.FieldByName('Vmj').AsString;
      aValue.Naz := fdqStavkeSelect.FieldByName('Naz').AsString;
      aValue.Napomena := fdqStavkeSelect.FieldByName('Napomena').AsString;
      aValue.BarCode := fdqStavkeSelect.FieldbyName('BarCode').AsString;
      aValue.Oper := fdqStavkeSelect.FieldByName('Oper').AsInteger;
      aValue.Vrijeme := fdqStavkeSelect.FieldByName('Vrijeme').AsDateTime;
      Result := True;
    end;
  finally
    fdqStavkeSelect.Close;
  end;
end;

function TDMGlavni.StavkeUpdate(aValue: TStavke): boolean;
begin
  fdqStavkeUpdate.ParamByName('Id').AsInteger := aValue.Id;
  fdqStavkeUpdate.ParamByName('DokId').AsInteger := aValue.DokId;
  fdqStavkeUpdate.ParamByName('Pod').AsInteger := aValue.Pod;
  fdqStavkeUpdate.ParamByName('St').AsInteger := aValue.St;
  fdqStavkeUpdate.ParamByName('Ul').AsCurrency := aValue.Ul;
  fdqStavkeUpdate.ParamByName('Nabcij').AsCurrency := aValue.Nabcij;
  fdqStavkeUpdate.ParamByName('MUl').AsCurrency := aValue.MUl;
  fdqStavkeUpdate.ParamByName('MNabcij').AsCurrency := aValue.MNabcij;
  fdqStavkeUpdate.ParamByName('MVrnab').AsCurrency := aValue.MVrnab;
  fdqStavkeUpdate.ParamByName('Mat').AsInteger := aValue.Mat;
  fdqStavkeUpdate.ParamByName('Vmj').AsString := aValue.Vmj;
  fdqStavkeUpdate.ParamByName('Naz').AsString := aValue.Naz;
  fdqStavkeUpdate.ParamByName('Napomena').AsString := aValue.Napomena;
  fdqStavkeUpdate.ParamByName('BarCode').AsString := aValue.BarCode;
  fdqStavkeUpdate.ParamByName('Oper').AsInteger := aValue.Oper;
  fdqStavkeUpdate.ParamByName('Vrijeme').AsDateTime := aValue.Vrijeme;
  try
    fdqStavkeUpdate.ExecSQL;
    Result := True
  finally
    Result := False;
  end;
end;

end.
